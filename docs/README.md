**the guide et al**: [top](https://klembot.github.io/chapbook/) | [guide](https://klembot.github.io/chapbook/guide/) | [github](https://github.com/klembot/chapbook/issues) | [discord](https://discord.com/channels/389867840406159362/501834019621830667)

**chapbook github examples**: https://github.com/klembot/chapbook/tree/main/examples. These are mostly now covered in the guide. Maybe look at
[battle.txt](https://github.com/klembot/chapbook/blob/main/examples/battle.txt).

**comments** You can also do HTML comments with `<!-- comment -->` syntax (see https://github.com/klembot/chapbook/blob/main/examples/comments.txt)

**JavaScript link to passage**: `<a href="javascript:void(0)" data-cb-go="Passage Name"><img src="URL /></a>`. (from klembot discord https://discord.com/channels/389867840406159362/501834019621830667/853014063402188820)

**JavaScript reveal link (passage)**: `'<a href="javascript:void(0)" data-cb-reveal-passage="' + name + '">' + (label ?? name) + '</a>'` (from [JennaS discord](https://discord.com/channels/389867840406159362/501834019621830667/878857413468495902))

**button link to passage**: `<button>[[OK->title]]</button>`

**global state**: Passing global state between JavaScript sections: `window.setup = window.setup || {};`. The you can hang things (e.g., functions and data) off setup and refer to it like `setup.my_func()`. (Recommended in https://twinery.org/cookbook/terms/terms_javascript.html. Shown in https://twinery.org/cookbook/fairmath/chapbook/chapbook_fairmath.html#twee-code.)

**on passage change**: Do something on every passage change. "In general, if you would like to run some JS every time the player navigates, it looks like this:" (from klembot discord https://discord.com/channels/389867840406159362/501834019621830667/735304144063037480)
```
    engine.event.on('state-change', ({name}) => {
      if (name === 'trail') {
        console.log('Do your thing here');
      }
    });
```

 **passage visited, for another passage**: (from https://discord.com/channels/389867840406159362/501834019621830667/769294263808557056)
```
otherVisited: trail.contains('Other Passage Name')
--
[if otherVisited]
You've been to "Other Passage Name" before.
```

**custom lookups**: from https://github.com/klembot/chapbook/issues/10#issuecomment-524687500
```
[JavaScript]
engine.extend('1.0.0', () => {
  engine.state.setLookup('timeOfDay', () => engine.state.get('hours') % 6);
  engine.state.setLookup('dayOfWeek', () => (Math.floor(engine.state.get('hours') / 6)) % 7);
});
```

**passage tags**: To access passage tags we must get the passage data for the
named passage (`'pen'` in this case).
```
[JavaScript]
const pd = document.querySelector("tw-passagedata[name='pen']");
if (pd) {
    write('The passage tags are: ' + pd.getAttribute('tags'));
}
else {
    write('INTERNAL ERROR: I could not find the pen passage data.');
}
[continue]
```

**dom-click, dom-change, and inserts** "The general pattern I’ve followed so far is to render out HTML with data attributes.
You can then subscribe to dom-click events in Chapbook and do stuff accordingly, or just imitate the reveal link HTML. `engine.event.add(‘dom-click’, el => console.log(‘clicked’, el))`
Right now Chapbook emits ‘dom-change’ (when an input changes) and ‘dom-click’ events"
(from [klembot discord](https://discord.com/channels/389867840406159362/501834019621830667/879030076904210513))
* [template/inserts/reveal-link.js](https://github.com/klembot/chapbook/blob/1.2.1/src/template/inserts/reveal-link.js)
* "This module emits `dom-change` and `dom-click` events on any DOM element that is
changed (i.e. inputs) or clicked with any `data-cb-*` attribute." [src/display/index.js](https://github.com/klembot/chapbook/blob/1.2.1/src/display/index.js)
